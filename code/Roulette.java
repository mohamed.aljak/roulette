import java.util.Scanner;

public class Roulette{
    public static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        start();
    }

    public static void start(){
        int balance = 1000;

        while(true){
            //Check if the user would like to play otherwise exit game
            boolean makeBet = askToMakeBet();
            if(makeBet == false){
                break;
            }

            //Get user's bet number and amount to bety
            int betNumber = askBetNumber();
            int howMuchToBet = askHowMuchToBet(balance);
            
            //Perform the bet and update balance;
            balance = balance + bet(betNumber, howMuchToBet);
            System.out.println("New balance: " + balance);
        }
        System.out.println("Your balance difference is: " + (balance - 1000));
    }

    public static boolean askToMakeBet(){
        System.out.println("Would you like to make a bet?");
        String userInput = scanner.nextLine();
        if(userInput.equals("yes")){
            return true;
        }
        else{
            return false;
        }
    }

    public static int askBetNumber(){
        while(true){
            System.out.println("What is your bet number?");
            int betNumber = Integer.parseInt(scanner.nextLine());
            if(betNumber < 0 || betNumber > 36){
                System.out.println("Bet number out of range!");
            } else {
                return betNumber;
            }
        }
    }

    public static int askHowMuchToBet(int balance){
        while(true){
        System.out.println("How much would you like to bet?");
        int howMuchToBet = Integer.parseInt(scanner.nextLine());
        if(howMuchToBet <= balance){
            return howMuchToBet;
        }
        System.out.println("Bet too large!");
        }
    } 

    public static int bet(int betNumber, int howMuchToBet){
        RouletteWheel rouletteWheel =  new RouletteWheel();
        int cashReward = 0;

        rouletteWheel.spin();
        int rouletteNumber = rouletteWheel.getValue();
        
        //Check IF win
        if(betNumber == rouletteNumber){
            cashReward = howMuchToBet * 35;
        } else {
            cashReward = -howMuchToBet;
        }
        
        return cashReward;
    }
}